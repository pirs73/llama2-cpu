from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
from langchain_community.llms import LlamaCpp

MODEL_PATH = "../llama-2-7b-chat.Q4_0.gguf"


# 1. Create a function to load Llama model
# 2. Create a function load prompt


def create_prompt():
    """Creates prompt template"""

    # Prompt copied from langchain docs
    _DEFAULT_TEMPLATE: str = """Assistant is a expert JSON builder designed to assist with a wide range of tasks.

    Assistant is able to respond to the User and use tools using JSON strings that contain "action" and 
    "action_input" parameters.
    
    All of Assistant's communication is performed using this JSON format.
    
    Assistant can also use tools by responding to the user with tool use instructions in the same "action" 
    and "action_input" JSON format. Tools available to Assistant are:
    
    - "Calculator": Useful for when you need to answer questions about math.

    Human: {question}
    Assistant:"""

    prompt: PromptTemplate = PromptTemplate(
        input_variables=["question"],
        template=_DEFAULT_TEMPLATE
    )

    return prompt


def load_model() -> LLMChain:
    """Load Llama model"""
    callback_manager: CallbackManager = CallbackManager([StreamingStdOutCallbackHandler()])

    # Make sure the model path is correct for your system!
    llama_model: LlamaCpp = LlamaCpp(
        model_path=MODEL_PATH,
        temperature=0.5,
        max_tokens=2000,
        top_p=1,
        callback_manager=callback_manager,
        verbose=True,  # Verbose is required to pass to the callback manager
    )

    prompt: PromptTemplate = create_prompt()

    llm_chain = LLMChain(
        llm=llama_model,
        prompt=prompt
    )

    return llm_chain


llm_chain = load_model()

prompt: str = """Question: What is the largest country on Earth?"""

response: str = llm_chain.run(prompt)
