from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain_community.llms import CTransformers

# MODEL_PATH = "../llama-2-7b-chat.Q4_0.gguf"
MODEL_PATH = "../llama-2-13b-chat.Q4_0.gguf"


# 1. Create a function to load Llama model
# 2. Create a function load prompt


def load_model() -> CTransformers:
    """Load Llama model"""

    callback_manager: CallbackManager = CallbackManager([StreamingStdOutCallbackHandler()])

    # Make sure the model path is correct for your system!
    llama_model: CTransformers = CTransformers(
        model=MODEL_PATH,
        model_type="llama",
        verbose=True,
        temperature=0.75,
        max_new_tokens=512,
        lib='avx',
        callback_manager=callback_manager,
    )

    return llama_model


llm = load_model()

model_prompt: str = """Question: What is the largest country on Earth?"""

response: str = llm(model_prompt)
